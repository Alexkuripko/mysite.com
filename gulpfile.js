const gulp = require('gulp');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const sass = require('gulp-sass');

sass.compiler = require('node-sass');

const cssFiles = [
    './app/**/*css',
    // './node_modules/node-normalize-scss/_normalize.scss',
];
const sassFiles = [
    './app/**/*.scss',
];

const jsFiles = [
    './app/**/*.js',
];

function sassStyles() {
    return gulp.src(sassFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./app/src/css'));
}
function styles() {
    return gulp.src(cssFiles)
        .pipe(concat('style.css'))
        .pipe(cleanCSS({
            level: 2
        }))
        .pipe(gulp.dest('./assets/build/css'));
}

function scripts() {
    return gulp.src(jsFiles)
        .pipe(concat('script.js'))
        .pipe(uglify({
            toplevel: true
        }))
        .pipe(gulp.dest('./assets/build/js'));
}

function watch(){
    gulp.watch('./app/views/**/*.css', styles);
    gulp.watch('./app/views/**/*.js', scripts);
}

function clean(){
    return del(['assets/build/*']);
}

gulp.task('sass', sassStyles);
gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('watch', watch);
gulp.task('build', gulp.series(clean,sassStyles,
    gulp.parallel(styles, scripts)

));
