<?php
namespace app\components\helpers;

class ProfileHelper
{

    public static function suffValidation($minuteDiff = null, $hourDiff = null, $dayDiff = null, $mounthDiff = null, $yearDiff = null)
    {
        $suff = '';

        if ($minuteDiff != null) {
            $symCount = iconv_strlen($minuteDiff);
            if ($symCount == 2) {
                $rest = substr($minuteDiff, 1);
            } else $rest = $minuteDiff;
            if ($rest == 1) {
                if (substr($minuteDiff, 0) == 1) {
                    $suff = '';
                } else $suff = 'у';
            }
            if ($rest > 1 && $rest < 5) {
                if (substr($minuteDiff, 0) == 1) {
                    $suff = '';
                } else $suff = 'ы';
            }
        }
        if ($hourDiff != null) {
            $symCount = iconv_strlen($hourDiff);
            if ($symCount == 2) {
                $rest = substr($hourDiff, 1);
            } else $rest = $hourDiff;
            if ($rest > 1 || $rest < 5) {
                $suff = 'а';
            }
            if ($hourDiff > 4 || $rest == 0) {
                $suff = 'ов';
            }
        }
        if ($dayDiff != null) {
            $symCount = iconv_strlen($dayDiff);
            if ($symCount == 2) {
                $rest = substr($dayDiff, 1);
            } else $rest = $dayDiff;
            if ($rest == 1) {
                $suff = 'день';
            } else if ($rest > 1 && $rest < 5) {
                $suff = 'дня';
            } else if ($rest > 4) {
                $suff = 'дней';
            }
        }
        if ($mounthDiff != null) {
            $symCount = iconv_strlen($mounthDiff);
            if ($symCount == 2) {
                $rest = substr($mounthDiff, 1);
            } else $rest = $mounthDiff;
            if ($rest > 1 && $rest < 5) {
                $suff = 'а';
            } else if ($rest > 4) {
                $suff = 'ев';
            }

        }
        if ($yearDiff != null) {
            $symCount = iconv_strlen($yearDiff);
            if ($symCount == 2) {
                $rest = substr($yearDiff, 1);
            } else $rest = $yearDiff;
            if ($rest == 1) {
                $suff = 'год';
            } else if ($rest > 1 && $rest < 5) {
                $suff = 'года';
            } else if ($rest == 0 || $rest > 4) {
                $suff = 'лет';
            }


        }
        return $suff;
    }

    public static function lastDateActive($date, $gender = 'm')
    {
        $lastActive = '';
        $gen = '';
        $timeNow = date('Y-m-d G:i:s');
        $datetime1 = date_create($timeNow);
        $datetime2 = date_create($date);
        $interval = date_diff($datetime2, $datetime1);
        $diff = $interval->format('%y/%m/%d/%h/%i/%s');
        $segments = explode('/', $diff);
        if ($gender == 'w') $gen = 'a';
        $yearDiff = $segments[0];
        $mounthDiff = $segments[1];
        $dayDiff = $segments[2];
        $hourDiff = $segments[3];
        $minuteDiff = $segments[4];
        if ($yearDiff == 0 && $mounthDiff == 0 && $dayDiff == 0 && $hourDiff == 0 && $minuteDiff <= 5) {
            $lastActive = 'в сети';
        } else if ($yearDiff == 0 && $mounthDiff == 0 && $dayDiff == 0 && $hourDiff == 0 && $minuteDiff > 5) {
            $lastActive = 'был' . $gen . ' в сети ' . $minuteDiff . ' минут' . self::suffValidation(
                    $minuteDiff, null, null, null, null) . ' назад';
        } else if ($yearDiff == 0 && $mounthDiff == 0 && $dayDiff == 0 && $hourDiff >= 1) {
            $lastActive = 'был' . $gen . ' в сети ' . $hourDiff . ' час' . self::suffValidation(
                    null, $hourDiff, null, null, null) . ' назад';
        } else if ($yearDiff == 0 && $mounthDiff == 0 && $dayDiff >= 1) {
            $lastActive = 'был' . $gen . ' в сети ' . $dayDiff . ' ' . self::suffValidation(
                    null, null, $dayDiff, null, null) . ' назад';
        } else if ($yearDiff == 0 && $mounthDiff >= 1) {
            $lastActive = 'был' . $gen . ' в сети ' . $mounthDiff . ' месяц' . self::suffValidation(
                    null, null, null, $mounthDiff, null) . ' назад';
        } else if ($yearDiff >= 1) {
            $lastActive = 'был' . $gen . ' в сети ' . $yearDiff . ' ' . self::suffValidation(
                    null, null, null, null, $yearDiff) . ' назад';
        }
        return $lastActive;
    }
}