<?php
namespace app\components\helpers;

class UserHelper
{

    public static function getButton($status, $id)
    {
        if ($status == 'in') {
            return '<p id="confirmFriend' . $id . '" class="friend-btn" onclick="confirmFriend(' . $id . ')"> подтвердить</p>';
        } elseif ($status == 'out') {
            return ' <p id="cancelRequest' . $id . '" class="friend-btn" onclick="cancelRequest(' . $id . ')">отменить заявку</p>';
        } elseif ($status == 'friend') {
            return '<p id="delFriend' . $id . '" class="friend-btn" onclick="delFriend(' . $id . ')"> удалить из друзей</p>';
        } elseif ($status == 'none') {
            return '<p id="addFriend' . $id . '" class="friend-btn" onclick="addFriend(' . $id . ')"> добавить в друзья</p>';
        }
    }
}