<?php
namespace app\components;

use app\lib\Db;
use PDO;

class Router
{

    private $routes;
    const BASE_URL = 'http://mysite.com/';

    public function __construct()
    {
        $routesPath = './app/config/routes.php';
        $this->routes = include($routesPath);

    }

    public static function redirect(string $controller = null, $action = null, array $params = [])
    {
        $url = self::BASE_URL;

        if ($controller) {
            $url .= $controller;
        }
        if ($action) {
            $url .= '/' . $action;
        }

        header("Location:" . $url);
    }

    public function updateUserActive($id)
    {
        $date = date('Y-m-d G:i:s');
        $db = Db::getConnection();
        $db->prepare('UPDATE `user` SET `date_of_last_active` = \'' . $date . '\' WHERE `user`.`id` LIKE ' . $id)->execute();
    }

    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }


    public function run()
    {
        $uri = $this->getURI();

        if (isset($_SESSION['user'])) {
            $this->updateUserActive($_SESSION['user']['id']);
        }
        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {

                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $segments = explode('/', $internalRoute);

                $controllerName = ucfirst(array_shift($segments) . 'Controller');
                $actionName = 'action' . ucfirst(array_shift($segments));
                $parameters = $segments;


                $controllerFile = './app/controllers/' . $controllerName . '.php';
                if (file_exists($controllerFile)) {
                    include_once($controllerFile);

                } else {
                    echo $controllerName . '  not found!';
                    exit;
                }
                $controllerObject = new $controllerName;
                $result = call_user_func_array([$controllerObject, $actionName], $parameters);

                if ($result != null) {
                    break;
                }
                return;

            }
        }
    }
}
