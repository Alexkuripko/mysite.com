<?php

namespace app\components\validators;

class ComparisonValidator extends Validator
{

    public function __construct()
    {

    }

    public function validate($data): bool
    {
        $valid = true;
        if (isset($_POST['pass'])) {
            if ($data != $_POST['pass']) {
                $valid = false;
                $this->messages = 'пароли не совпадают';
            }
        }
        return $valid;
    }
}