<?php

namespace app\components\validators;

class ComparisonValidatorEdit extends Validator
{

    public function validate($data): bool
    {
        $valid = true;
        if ($_POST != null) {
            if ($data != $_POST['new_pass']) {
                $valid = false;
                $this->messages = 'Passwords do not match';
            }
        }
        return $valid;
    }
}