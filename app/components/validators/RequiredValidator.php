<?php

namespace app\components\validators;

class RequiredValidator extends Validator
{
    public function validate($data): bool
    {
        $valid = !is_null($data);
        if (is_null($data)) {
            $this->messages = 'это поле необходимо заполнить!';
        }

        return $valid;
    }
}