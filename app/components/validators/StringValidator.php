<?php

namespace app\components\validators;

class StringValidator extends Validator
{
    private $min;
    private $max;

    public function __construct($min = 0,  $max = 0)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function validate($data): bool
    {
        $valid = is_string($data);
        if (strlen($data) >= $this->min and strlen($data) <= $this->max) {
            $valid = true;
        } else {
            $valid = false;
        }
        if (!$valid) {
            $min_num = $this->min;
            $max_num = $this->max;

            $min_suff = $this->getSuff($min_num);
            $max_suff = $this->getSuff($max_num);

            $this->messages = 'не должно быть короче ' . $min_num . '-' . $min_suff . ' и длиннее ' . $max_num . '-' . $max_suff . ' символов';
        }

        return $valid;
    }

    public function getSuff($num)
    {
        $suff = '';
        if ($num < 5) $suff = 'х';
        if (($num > 4 && $num < 7) || $num > 8) $suff = 'ти';
        if ($num > 6 && $num < 9) $suff = 'ми';
        return $suff;
    }
}