<?php

namespace app\components\validators;

use app\models\Account;

class UniqueLogin extends Validator
{
    public function validate($login): bool
    {
        $user = new Account();
        $this->messages = 'этот логин занят!';
        return $user->getDataByLogin($login) == null;

    }
}