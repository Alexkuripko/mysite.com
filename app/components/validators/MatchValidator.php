<?php

namespace app\components\validators;

class MatchValidator extends Validator
{
    private $mask;

    public function __construct(string $mask)
    {
        $this->mask = $mask;
    }

    public function validate($data): bool
    {
        $valid = preg_match($this->mask, $data);
        if (!preg_match($this->mask, $data)) {
            $this->messages = 'поле содержит некорректные символы';
        }

        return $valid;
    }
}