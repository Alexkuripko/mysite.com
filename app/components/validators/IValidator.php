<?php

namespace app\components\validators;

interface IValidator
{

    public function validate($data): bool;

}