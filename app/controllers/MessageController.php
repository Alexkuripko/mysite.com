<?php
/**
 * Created by PhpStorm.
 * User: alexandr
 * Date: 17.02.19
 * Time: 16:58
 */

use app\controllers\BaseController;
use app\models\Message;
use app\models\Profile;
use app\components\Router;


class MessageController extends BaseController
{
    public $model;

    public function __construct()
    {
        $this->model = new Message();
    }

    public function actionMessages()
    {
        if (isset($_SESSION['user'])) {
            $dialogueList = $this->model->getDialogueList();
            $this->setLayout('default');
            $userList = $this->model->getUserList(true);

            $this->render('messages', [
                'userList' => $userList,
                'dialogueList' => $dialogueList
            ]);
        } else {
            Router::redirect("login");
        }
        return true;
    }

    function actionCorrespondence($userLogin = null)
    {
        if (!isset($_SESSION['user']['id'])) {
            Router::redirect('login');
            return;
        }
        if (!$userLogin) {
            Router::redirect('login');
            return;
        }
        $id = $this->model->getUserData($userLogin, 'login', 'id');
        $avatar = $this->model->getUserData($id, 'id', 'avatar');
        $name = $this->model->getUserData($id, 'id', 'name');
        $lastname = $this->model->getUserData($id, 'id', 'lastname');
        $date = $this->model->getUserData($id, 'id', 'date_of_last_active');
        $gender = $this->model->getUserData($id, 'id', 'gender');
        $messageList = $this->model->getMessageList($id);
        $actionName = 'correspondence';
        $this->setLayout('default');
        $this->render($actionName, [
            'messageList' => $messageList,
            'login' => $userLogin,
            'name' => $name,
            'lastname' => $lastname,
            'avatar' => $avatar,
            'date' => $date,
            'gender' => $gender
        ]);
    }

    public function actionSendMessage()
    {
        if (isset($_POST['message'])) {
            $sendData = $this->model->prepareSendData($_POST);
            $this->model->sendMessage($sendData);
            $this->model->updateDialogueInfo($_POST);
            Router::redirect("correspondence/" . $_SESSION['last_login']);
        } else {
            Router::redirect("profile/" . $_SESSION['last_login']);
        }
    }

    public function actionCorrList()
    {
        if (isset($_POST['userLogin'])) {
            $userLogin = $_POST['userLogin'];
            $userList = $this->model->getUserList();
            foreach ($userList as $index) {
                foreach ($index as $item => $value) {
                    if ($item == 'login' && $value == $userLogin) {
                        extract($index);
                    }
                }
            }
            $messageList = $this->model->getMessageList($id);
            include 'app/views/message/corrList.php';
        }

    }

    public function actionDellMessage()
    {
        $this->model->dellMessage($_POST['id']);
    }
}
