<?php

namespace app\controllers;

class BaseController
{
    protected $layoutName = 'default';
    private $title;
    const TITLES_PATH = './app/config/titles.php';

    public function __construct()
    {

    }

    public function pagesAccess(): bool
    {
        return isset($_SESSION['user']);
    }

    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function addTitle()
    {
        $this->title = include_once(BaseController::TITLES_PATH);
        $uri = $this->getURI();
        foreach ($this->title as $key => $title) {
            if (preg_match("~$key~", $uri)) {
                $this->title = $title;
                echo($this->title);
                return;
            }

        }
    }

    public function setLayout($value)
    {
        $this->layoutName = $value;
        return $this;
    }

    function render($actionName, $data = null)
    {
        if (is_array($data)) {
            extract($data);
        }
        if (($actionName == 'profile') || ($actionName == 'photo')) {
            extract($user);
            foreach ($user as $datum) {
                extract($datum);
            }
        }
        $controller = static::class;
        $controllerName = lcfirst(substr($controller, 0, strpos($controller, 'Controller')));
        $content = './app/views/' . $controllerName . '/' . $actionName . '.php';
        $layoutFile = $this->layoutName . '.php';

        include_once './app/views/layouts/' . $layoutFile;
    }

}