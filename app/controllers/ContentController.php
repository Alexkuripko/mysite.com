<?php

use app\controllers\BaseController;
use app\components\Router;
use app\models\Content;

class ContentController extends BaseController
{
    public $model;

    public function __construct()
    {

        $this->model = new Content();
    }

    public function actionNewsTape()
    {
        if (isset($_SESSION['user'])) {
            $this->setLayout('default');
            $this->render('newsTape');
        } else {
            Router::redirect("login");
        }
        return true;
    }

    public function actionPhoto($userLogin = null)
    {
        if (isset($_SESSION['user'])) {
            $userLogin = $userLogin ?? $_SESSION['user']['login'] ?? null;
            $id = $this->model->getUserData($userLogin, 'login', 'id');
            $photos = $this->model->getPhotos($id);
            $user = $this->model->getUserData($userLogin, 'login', 'user');
            $photoLikesUser = $this->model->getPhotoLikes($_SESSION['user']['id'], 'user_id');
            $actionName = 'photo';
            $this->setLayout('default');

            $this->render($actionName, [
                'user' => $user,
                'photos' => $photos,
                'photoLikesUser' => $photoLikesUser,
            ]);
        } else {
            Router::redirect("login");
        }
    }

    public function actionProfilePhotos()
    {
        if (isset($_SESSION['user'])) {
            $userId = $_POST['id'];
            $userList = $this->model->getUserList(true);
            $photoLikesUser = $this->model->getPhotoLikes($_SESSION['user']['id'], 'user_id');
            $name = $userList[$userId]['name'];
            $lastname = $userList[$userId]['lastname'];
            $login = $userList[$userId]['login'];
            $avatar = $userList[$userId]['avatar'];
            $photos = $this->model->getPhotos($userId);
            include 'app/views/content/profilePhotos.php';
        } else {
            Router::redirect("login");
        }
    }

    public function actionPhotoTiles()
    {
        if (isset($_SESSION['user'])) {
            $userId = $_POST['id'];
            $userList = $this->model->getUserList(true);
            $photoLikesUser = $this->model->getPhotoLikes($_SESSION['user']['id'], 'user_id');
            $name = $userList[$userId]['name'];
            $lastname = $userList[$userId]['lastname'];
            $login = $userList[$userId]['login'];
            $avatar = $userList[$userId]['avatar'];
            $photos = $this->model->getPhotos($userId);

            include 'app/views/content/photoTiles.php';
        } else {
            Router::redirect("login");
        }
    }

    public function actionLoadAvatar()
    {
        if (isset($_SESSION['user'])) {
            $name = 'avatar';
            $dir = '/app/template/images/avatar';

            if ($_FILES['avatar']['size'] > 0) {
                $this->model->loadFile($dir, $name);
                $this->model->updateAvatarInfo();
            }

            Router::redirect('profile/' . $_SESSION['user']['login']);
        } else {
            Router::redirect("login");
        }
    }

    public function actionLoadPhoto()
    {
        if (isset($_SESSION['user'])) {
            $filename = 'photo';
            $baseDir = $_SERVER['DOCUMENT_ROOT'];
            $dir = '/app/template/images/photo/photo_' . $_SESSION['user']['login'];

            if (!file_exists($baseDir . $dir)) {
                mkdir($baseDir . $dir, 0755, true);
            }

            if (isset($_FILES['photo'])) {
                $filename = $this->model->loadFile($dir, $filename);
                $this->model->addPhotoInDb($filename);
            }

            Router::redirect('profile/' . $_SESSION['user']['login']);
        } else {
            Router::redirect("login");
        }
    }

    public function actionDellPhoto()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_POST['name'])) {
                $dir = '/app/template/images/photo/photo_' . $_SESSION['user']['login'] . '/';
                $fileName = $_POST['name'];
                $this->model->deleteFile($dir, $fileName);
                $this->model->dellPhotoOfDb($fileName);
            }
        } else {
            Router::redirect("login");
        }

    }

    public function actionPutLike()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_POST['photoId'])) {

                $this->model->putLikePhoto();
            }
        } else {
            Router::redirect("login");
        }
    }

    public function actionRemoveLike()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_POST['photoId'])) {

                $this->model->removeLikePhoto();
            }
        } else {
            Router::redirect("login");
        }
    }
}
