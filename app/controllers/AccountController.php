<?php

use app\controllers\BaseController;
use app\lib\Db;
use app\models\Account;
use app\components\Router;

class AccountController extends BaseController
{

    public $model;

    function __construct()
    {
        $this->model = new Account();
    }

    public function actionLogin()
    {
        $login = '';
        $password = '';
        $error = false;
        if (!isset($_SESSION['user'])) {
            if (isset($_POST['submit'])) {
                $login = $_POST['login'];
                $password = $_POST['pass'];

                $userId = $this->model->checkUserData($login, $password);

                if (!$userId) {
                    $error = 'Неправильные данные для входа';
                } else {
                    $this->model->auth($userId);
                    Router::redirect('profile/' . $_SESSION['user']['login']);
                }
            }
            $this->setLayout('account');
            $this->render('login', [
                'error' => $error,
                'login' => $login,
                'password' => $password
            ]);
        } else {
            Router::redirect('profile/' . $_SESSION['user']['login']);
        }
    }

    public function actionLogout()
    {
        session_unset();
        Router::redirect('login');
    }

    public function actionRegister()
    {
        $name = '';
        $lastname = '';
        $login = '';
        $password = '';
        $passConfirm = '';
        if (!isset($_SESSION['user'])) {
            if (isset($_POST['submit'])) {
                $name = $_POST['name'];
                $lastname = $_POST['lastname'];
                $login = $_POST['login'];
                $password = $_POST['pass'];
                $passConfirm = $_POST['pass2'];

                if ($this->model->validate($_POST)) {
                    $this->model->addNewUser($_POST);
                    Router::redirect('login');
                }
            }
            $errors = $this->model->getErrors();
            $this->setLayout('account');
            $this->render('register', [
                'errors' => $errors,
                'name' => $name,
                'lastname' => $lastname,
                'login' => $login,
                'password' => $password,
                'password2' => $passConfirm,
            ]);
        } else {
            Router::redirect('login');
        }
    }
}