<?php

use app\controllers\BaseController;
use app\components\Router;
use app\models\User;

class UserController extends BaseController
{
    public $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function actionFriend()
    {
        if ($this->pagesAccess()) {
            $userList = $this->model->getUserList();
            $userList = $this->model->converseUserList($userList);
            $this->setLayout('default');

            $this->render('friend', [
                'userList' => $userList,
            ]);
        } else {
            Router::redirect('login');
        }
    }

    public function actionUsers()
    {
        $userList = $this->model->getUserList();
        $userList = $this->model->converseUserList($userList);
        $this->setLayout('default');

        $this->render('users', [
            'userList' => $userList,
        ]);
    }

    public function actionAddFriend()
    {
        if ($this->pagesAccess()) {
            $this->model->addFriend($_POST['user']);
        } else {
            Router::redirect('login');
        }
    }

    public function actionConfirmFriend()
    {
        if ($this->pagesAccess()) {
            $this->model->confirmFriend($_POST['user']);
        } else {
            Router::redirect('login');
        }
    }

    public function actionCancelRequest()
    {
        if ($this->pagesAccess()) {
            $this->model->delFriend($_POST['user']);
        } else {
            Router::redirect('login');
        }
    }

    public function actionDelFriend()
    {
        if ($this->pagesAccess()) {
            $this->model->delFriend($_POST['user']);
        } else {
            Router::redirect('login');
        }
    }

    public function actionFriendList()
    {
        if ($this->pagesAccess()) {
            $userList = $this->model->getUserList();
            $userList = $this->model->converseUserList($userList);
            include 'app/views/user/friendList.php';
        } else {
            Router::redirect('login');
        }
    }

    public function actionFriendInRequest()
    {
        if ($this->pagesAccess()) {
            $userList = $this->model->getUserList();
            $userList = $this->model->converseUserList($userList);
            include 'app/views/user/friendInRequest.php';
        } else {
            Router::redirect('friend');
        }
    }

    public function actionFriendOutRequest()
    {
        if ($this->pagesAccess()) {
            $userList = $this->model->getUserList();
            $userList = $this->model->converseUserList($userList);
            include 'app/views/user/friendOutRequest.php';
        } else {
            Router::redirect('login');
        }
    }
}