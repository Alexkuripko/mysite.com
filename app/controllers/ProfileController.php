<?php

use app\controllers\BaseController;
use app\models\Profile;
use app\models\Account;
use app\models\Content;
use app\components\Router;

class ProfileController extends BaseController
{

    public $model;
    public $model2;

    public function __construct()
    {

        $this->model = new Content();
    }


    function actionIndex($userLogin = null)
    {
        $login = $login ?? $_SESSION['user']['login'] ?? null;
        if (!isset($_SESSION['user']['id'])) {
            Router::redirect('login');
            return;
        }

        if (!$userLogin) {
            Router::redirect('login');
            return;
        }

        if (!$user = $this->model->getUserData($userLogin, 'login', 'user')) {
            Router::redirect('error');
            return;
        }

        $id = $this->model->getUserData($userLogin, 'login', 'id');
        $photos = $this->model->getPhotos($id);
        $photoLikesUser = $this->model->getPhotoLikes($_SESSION['user']['id'], 'user_id');
        $photoLikesOwner = $this->model->getPhotoLikes($id, 'owner_id');
        $actionName = 'profile';
        $this->setLayout('default');

        $this->render($actionName, [
            'user' => $user,
            'photos' => $photos,
            'photoLikesUser' => $photoLikesUser
        ]);
    }

    public function actionEdit()
    {
        $this->model = new Profile();
        if (isset($_SESSION['user']['id'])) {
            $this->model2 = new Account();
            $id = $_SESSION['user']['id'];
            $user = $this->model2->getUserById($id);
            $name = $user[0]['name'];
            $lastname = $user[0]['lastname'];

            if (isset($_POST['submit'])) {
                $name = $_POST['name'];
                $lastname = $_POST['lastname'];
                $password = $_POST['new_pass'];
                if ($this->model->validateEdit($_POST)) {
                    $id = $_SESSION['user']['id'];
                    $this->model->edit($id, $name, $lastname, $password);
                    Router::redirect('logout');
                    return true;
                }
            }
            $errors = $this->model->getErrors();
            $actionName = 'edit';
            $this->setLayout('default');
            $this->render($actionName, [
                'name' => $name,
                'lastname' => $lastname,
                'errors' => $errors,
            ]);
        } else Router::redirect('login');
    }
}