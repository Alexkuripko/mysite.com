<link rel="stylesheet" href="/app/views/content/style/fullPhoto.css">

<div class="full_photo" id="full_photo<?= $id ?>">
    <div id="photo_popup">
        <i class="fas fa-bars" id="open_photo_menu-btn" onclick="openPhotoMenu('<?= $id ?>')"></i>
        <div class="photo_menu" id="<?= $id ?>">
            <?php if ($login == $_SESSION['user']['login']): ?>
                <p class="photo_menu-btn" onclick="dellPhoto('<?= $photo_name ?>')">удалить</p>
            <?php endif; ?>
        </div>
        <img id="image_full_photo" src="/app/template/images/photo/photo_<?= $photoName ?>">
    </div>
    <div id="photo_info">
        <div id="user_info">
            <a href="/profile/<?= $login ?>"><img src="/app/template/images/avatar/<?= $avatar ?>"
                                                  class="rounded-circle" id="photo_user_avatar" width="50" height="50"></a>
            <div id="photo_user_name">
                <a href="/profile/<?= $login ?>" style="color: #4a555f"><?= $name ?> <?= $lastname ?></a>
            </div>
            <div id="photo_user_date">
                <p><?= $photo_date ?></p>
            </div>
            <hr id="photo_user_hr">
            <div>
                <?php
                $photoId = $id;
                $likeStatus = false;
                foreach ($photoLikesUser as $index) {
                    extract($index);
                    if (($photoId == $photo_id) && ($user_id == $_SESSION['user']['id'])) {
                        $likeStatus = true;
                    }
                }
                ?>
                <?php if ($likeStatus): ?>
                    <i class="fas fa-heart remote-btn<?= $photoId ?>"
                       onclick="removeLikePhoto('<?= $photoId ?>','<?= $_SESSION['last_id'] ?>')"
                       id="remove_like-btn"></i>
                    <i class="far fa-heart put-btn<?= $photoId ?>" style="display: none"
                       onclick="putLikePhoto('<?= $photoId ?>','<?= $_SESSION['last_id'] ?>')"
                       id="put_like-btn"></i>
                <?php else: ?>
                    <i class="far fa-heart put-btn<?= $photoId ?>"
                       onclick="putLikePhoto('<?= $photoId ?>','<?= $_SESSION['last_id'] ?>')"
                       id="put_like-btn"></i>
                    <i class="fas fa-heart remote-btn<?= $photoId ?>" style="display: none"
                       onclick="removeLikePhoto('<?= $photoId ?>','<?= $_SESSION['last_id'] ?>')"
                       id="remove_like-btn"></i>
                <?php endif; ?>
                <div class="likes" id="like_count<?= $photoId ?>"><?= $likes ?></div>
            </div>
        </div>
    </div>
    <i class="fas fa-times " id="close_photo-btn" onclick="closePhoto(<?= $photoId ?>)"></i>
</div>