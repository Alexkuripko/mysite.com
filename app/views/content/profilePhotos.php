<?php $i = 0 ?>
<?php foreach ($photos as $photo): ?>
    <?php if ($i < 4): ?>
        <?php $i++ ?>
        <?php extract($photo) ?>
        <?php $photoName = $login . '/' . $photo_name ?>
        <div class="photo" onclick="openPhoto('<?= $photoName ?>','<?= $id ?>')">
            <img src="/app/template/images/photo/photo_<?= $photoName ?>">
        </div>
        <?php include "app/views/content/fullPhoto.php" ?>
    <?php endif; ?>
<?php endforeach; ?>
<?php if (!sizeof($photos)): ?>
    <?php if ($_SESSION['user']['login'] == $login): ?>
        <p id="no_photos">здесь будут ваши фотографии</p><br>
    <?php else: ?>
        <p id="no_photos">пока нет фотографий</p><br>
    <?php endif; ?>
<?php endif; ?>