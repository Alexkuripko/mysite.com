<?php foreach ($photos as $photo): ?>
    <?php extract($photo) ?>
    <?php $photoName = $login . '/' . $photo_name ?>
    <div class="photo" onclick="openPhoto('<?= $photoName ?>','<?= $id ?>')"
         onmouseover="showNumOfLikes('<?= $id ?>')" onmouseout="hideNumOfLikes('<?= $id ?>')">
        <img src="/app/template/images/photo/photo_<?= $photoName ?>">
        <div class="photo_img" id="id<?= $id ?>">
            <div class="like_icon"><i class="fas fa-heart"></i></div>
            <span class="like_count"><?= $likes ?></span>
        </div>
    </div>
    <?php include "app/views/content/fullPhoto.php" ?>
<?php endforeach; ?>