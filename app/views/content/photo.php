<link rel="stylesheet" href="/app/views/content/style/photos.css">
<script src="/app/views/content/scripts/content.js"></script>
<link rel="stylesheet" href="/app/lib/css/progressbar.css">

<div class="content">
    <div class="content-block photos">
        <div id="userId" style="display: none"><?= $id ?></div>
        <?php if ($login == $_SESSION['user']['login']): ?>
            <p class="photos_title">Мои фотографии</p>
            <p onclick="showBlock('#addPhotoWindow')" id="add_photo-btn">добавить фотографию&nbsp;<i
                        class="fas fa-camera"> </i></p>
            <div class="content-block" id="addPhotoWindow">
                <div id="load_photo-form">
                    <input class="load_avatar" type="file" id="photo_file" name="photo">
                    <button class="load_avatar" id="load_avatar-btn" onclick="loadPhoto()">Загрузить</button>
                </div>
                <div id="load_photo-progress" style="display:none;">
                    <progress class='photo_progressbar progressbar' value='0' max='100'></progress>
                </div>
            </div>
        <?php else: ?>
            <p class="photos_title">Фотографии</p>
        <?php endif; ?>
        <hr>
        <div id="photo_tiles">
            <?php include "app/views/content/photoTiles.php" ?>
        </div>
    </div>
</div>