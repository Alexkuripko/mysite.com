function getPhotos() {
    var id = $('#userId').text();
    $.ajax({
        url: '/profilePhotos',
        type: 'POST',
        data: {
            id: id
        },
        success: function (data) {
            $("#photos").html(data);
        }
    })
}

function openPhoto(name, id) {
    $('#full_photo' + id).css('display', 'block');
    $('#image_full_photo').attr({'src': "/app/template/images/photo/photo_" + name});
}

function closePhoto(id) {
    $('#full_photo' + id).css('display', 'none');
}

function openPhotoMenu(id) {
    $('#' + id).css('display', 'block');
}

$(document).mouseup(function (e) {
    var block = $(".photo_menu");
    if (!block.is(e.target)
        && block.has(e.target).length === 0) {
        block.hide();
    }
});

function dellPhoto(name) {
    $.ajax({
        url: "/dellPhoto",
        type: "POST",
        data: {
            name: name
        }
    });
    getPhotos();
    getPhotoTiles();
}

function loadPhoto() {
    var data = new FormData();
    $.each($('#photo_file')[0].files, function (i, file) {
        data.append('photo', file);
    });
    $.ajax({
        url: '/loadPhoto',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function () {
            photoProgress();
        }
    });
}

function showNumOfLikes(id) {
    $('#id' + id).css('display', 'block');
}

function hideNumOfLikes(id) {
    $('#id' + id).css('display', 'none');
}

function putLikePhoto(photoId, userId) {
    $.ajax({
        url: "/putLike",
        type: "POST",
        data: {
            photoId: photoId,
            userId: userId
        }
    });
    var likeCount = $('#like_count' + photoId).text();
    likeCount = Number(likeCount);
    $('#like_count' + photoId).text(likeCount + 1);
    $('.put-btn' + photoId).css('display', 'none');
    $('.remote-btn' + photoId).css('display', 'block');

}

function removeLikePhoto(photoId, userId) {
    $.ajax({
        url: "/removeLike",
        type: "POST",
        data: {
            photoId: photoId,
            userId: userId
        }
    });
    var likeCount = $('#like_count' + photoId).text();
    likeCount = Number(likeCount);
    $('#like_count' + photoId).text(likeCount - 1);
    $('.put-btn' + photoId).css('display', 'block');
    $('.remote-btn' + photoId).css('display', 'none');

}

function getPhotoTiles() {
    var id = $('#userId').text();
    $.ajax({
        url: '/photoTiles',
        type: 'POST',
        data: {
            id: id
        },
        success: function (data) {
            $("#photo_tiles").html(data);
        }
    })
}

$(document).mouseup(function (e) {
    var block = $("#addPhotoWindow");
    if (!block.is(e.target)
        && block.has(e.target).length === 0) {
        block.hide();
    }
});

function photoProgress() {
    var p = 1;
    $("#load_photo-form").css('display', 'none');
    $("#load_photo-progress").css('display', 'block');
    var progressInterval = setInterval(function () {
        $(".progressbar").attr({'value': p});
        p++;
    }, 10);
    setTimeout(function () {
        clearInterval(progressInterval);
        $("#load_photo-form").css('display', 'block');
        $("#load_photo-progress").css('display', 'none');
        $('#addPhotoWindow').css('display', 'none');
        getPhotos();
        getPhotoTiles();
    }, 1000)
}


