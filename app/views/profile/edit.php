<link rel="stylesheet" href="/app/views/account/style/account.css">
<link rel="stylesheet" href="/app/views/profile/style/profile.css">
<div class="content">
    <div class="content-block content-edit">
        <div class="frm">
            <h3>редактирование данных</h3>
            <form method="post"><br>
                <?php if (isset($errors['name'])): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $errors['name']; ?>
                    </div>
                <?php endif; ?>
                <input type="text" name="name" placeholder="Введите имя:" value="<?= $name ?>" minlength="3"
                       maxlength="20" class="form_control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default" required>
                <?php if (isset($errors['lastname'])): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $errors['lastname']; ?>
                    </div>
                <?php endif; ?>
                <input type="text" name="lastname" placeholder="Введите фамилию:" value="<?= $lastname ?>"
                       minlength="3"
                       maxlength="20" class="form_control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default" required>
                <?php if (isset($errors['pass'])): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $errors['pass']; ?>
                    </div>
                <?php endif; ?>
                <input type="password" name="new_pass" placeholder="Придумайте пароль:"
                       minlength="8" maxlength="20" class="form_control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default" required>
                <button type="submit" name="submit" class="btn-lt">сохранить</button>
                <a href="/" class="btn-rt" style="text-decoration: none">отмена</a>
            </form>
        </div>
    </div>
</div>