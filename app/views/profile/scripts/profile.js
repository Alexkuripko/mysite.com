$(document).mouseup(function (e) {
    var block = $("#addPhotoWindow");
    if (!block.is(e.target)
        && block.has(e.target).length === 0) {
        block.hide();
    }
});

$(document).mouseup(function (e) {
    var block = $("#sendMessageWindow");
    if (!block.is(e.target)
        && block.has(e.target).length === 0) {
        block.hide();
    }
});

$(document).mouseup(function (e) {
    var block = $("#loadAvatarWindow");
    if (!block.is(e.target)
        && block.has(e.target).length === 0) {
        block.hide();
    }
});

function loadAvatar() {
    var data = new FormData();
    $.each($('#avatar_file')[0].files, function (i, file) {
        data.append('avatar', file);
    });
    $.ajax({
        url: '/loadAvatar',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: avatarProgress()
    });
}

function avatarProgress() {
    var p = 1;
    $("#load_avatar-form").css('display', 'none');
    $("#load_avatar-progress").css('display', 'block');
    setInterval(function () {
        $(".progressbar").attr({'value': p});
        p++;
        if (p === 100) {
            window.location.reload(true);
        }
    }, 10);

}


