<script src="/app/views/profile/scripts/profile.js"></script>
<script src="/app/views/content/scripts/content.js"></script>
<?php $_SESSION['last_login'] = $login ?>
<?php $_SESSION['last_id'] = $id ?>
<div id="userId" style="display: none"><?= $id ?></div>
<div class="content">
    <div class="content-block avatar">
        <div class="avatar_photo">
            <img src="/app/template/images/avatar/<?= $avatar ?>" onclick="showBlock('#full_avatar', 200)">
        </div>
        <div id="full_avatar" onclick="hideBlock('#full_avatar', 200)">
            <div id="avatar_popup">
                <img src="/app/template/images/avatar/<?= $avatar ?>" id="image_full_avatar">
            </div>
        </div>
        <?php if ($_SESSION['user']['login'] == $login): ?>
            <span class="button new_ava" onclick="showBlock('#loadAvatarWindow')">редактировать</span>
            <div class="content-block" id="loadAvatarWindow" style="display: none;">
                <div id="load_avatar-form">
                    <label for="upload-photo"><input class="load_avatar-btn" id="avatar_file" type="file" name="avatar"><br></label>
                    <input onclick="loadAvatar()" class="load_avatar-btn" type="submit" value="Загрузить"><br>
                </div>
                <div id="load_avatar-progress" style="display:none;">
                    <progress class='avatar_progressbar progressbar' value='0' max='100'></progress>
                </div>
            </div>
        <?php else: ?>
            <span class="button write_btn" onclick="showBlock('#sendMessageWindow',200)">написать</span>
            <div class="content-block" id="sendMessageWindow" style="display: none;">
                <form action="/sendMessage" method="post">
                    <textarea name="message" id="sendMessageWindowInput" cols="29" rows="6"></textarea></p>
                    <p><input class="button send_btn" type="submit" value="Отправить"></p>
                </form>
            </div>
        <?php endif; ?>
    </div>
    <div class="content-block user_information">
        <span id="user_information_name"><?= $name ?> <?= $lastname ?></span>
        <div id="active">
            <?= \app\components\helpers\ProfileHelper::lastDateActive($date_of_last_active, $gender) ?>
        </div>
        <hr/>
    </div>
    <div class="content-block user_photos">
        <?php if ($_SESSION['user']['login'] == $login): ?>
            <a href="/photo/<?= $login ?>" id="all_photos-btn">все фотографии</a>
            <p onclick="showBlock('#addPhotoWindow')" id="add_photo-btn">добавить фотографию&nbsp;<i class="fas fa-camera"> </i></p>
            <hr id="photo_hr">
        <?php else: ?>
            <a href="/photo/<?= $login ?>" id="all_photos-btn">все фотографии</a>
            <p id="add_photo-btn">&nbsp;</p>
            <hr id="photo_hr">
        <?php endif; ?>
        <div id="photos">
            <?php include "app/views/content/profilePhotos.php" ?>
        </div>
        <div class="content-block" id="addPhotoWindow">
            <div id="load_photo-form">
                <input class="load_avatar" type="file" id="photo_file" name="photo">
                <button class="load_avatar" id="load_avatar-btn" onclick="loadPhoto()">Загрузить</button>
            </div>
            <div id="load_photo-progress" style="display:none;">
                <progress class='photo_progressbar progressbar' value='0' max='100'></progress>
            </div>
        </div>
        <div class="content-block user_post">

        </div>
    </div>
</div>
