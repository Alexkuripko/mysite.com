<div class="header">
    <a href="#"></a>
</div>
<div class="frm">
    <form method="post"><br>
        <?php if (isset($errors['name'])): ?>
            <div class="alert alert-danger" role="alert">
                <?= $errors['name']; ?>
            </div>
        <?php endif; ?>
        <input type="text" name="name" placeholder="Введите имя:" value="<?php echo $name ?>" minlength="3"
               maxlength="25" class="form_control" aria-label="Sizing example input"
               aria-describedby="inputGroup-sizing-default" required>
        <?php if (isset($errors['lastname'])): ?>
            <div class="alert alert-danger" role="alert">
                <?= $errors['lastname']; ?>
            </div>
        <?php endif; ?>
        <input type="text" name="lastname" placeholder="Введите фамилию:" value="<?php echo $lastname ?>"
               minlength="3"
               maxlength="25" class="form_control" aria-label="Sizing example input"
               aria-describedby="inputGroup-sizing-default" required>
        <?php if (isset($errors['login'])): ?>
            <div class="alert alert-danger" role="alert">
                <?= $errors['login']; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($errors['login_exist'])): ?>
            <div class="alert alert-danger" role="alert">
                <?= $errors['login_exist']; ?>
            </div>
        <?php endif; ?>
        <input type="text" name="login" placeholder="Придумайте логин:" value="<?php echo $login ?>" minlength="5"
               maxlength="25" class="form_control" aria-label="Sizing example input"
               aria-describedby="inputGroup-sizing-default" required>
        <div>
            <?php if (isset($errors['pass'])): ?>
                <div class="alert alert-danger" role="alert">
                    <?= $errors['pass']; ?>
                </div>
            <?php endif; ?>
            <input type="password" name="pass" placeholder="Придумайте пароль:" value="<?= $password ?>"
                   minlength="8" maxlength="25" class="form_control" aria-label="Sizing example input"
                   aria-describedby="inputGroup-sizing-default" required>
            <?php if (isset($errors['pass2'])): ?>
                <div class="alert alert-danger" role="alert">
                    <?= $errors['pass2']; ?>
                </div>
            <?php endif; ?>
            <input type="password" name="pass2" placeholder="Повторите пароль:" value="<?= $password2 ?>"
                   class="form_control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            <div class="select_date" required>
                <p> дата рождения:<br>
                    <select name="born_day" class="select_elem">
                        <?php for ($i = 01; $i < 32; $i++) {
                            echo '<option>' . $i . '</option>';
                        } ?>
                    </select>
                    <select name="born_mounth" class="select_elem">
                        <option value="01">январь</option>
                        <option value="02">февраль</option>
                        <option value="03">март</option>
                        <option value="04">апрель</option>
                        <option value="05">май</option>
                        <option value="06">июнь</option>
                        <option value="07">июль</option>
                        <option value="08">август</option>
                        <option value="09">сентябрь</option>
                        <option value="10">октябрь</option>
                        <option value="11">ноябрь</option>
                        <option value="12">декабрь</option>
                    </select>
                    <select name="born_year" class="select_elem">
                        <?php for ($i = 2019; $i > 1919; $i--) {
                            echo '<option>' . $i . '</option>';
                        } ?>
                    </select></p>
            </div>
            <div class="select_gender">
                <p>выберите пол:<br>
                    <input id="m_gender" type="radio" name="gender" value="m" checked> Мужской
                    <input id="w_gender" type="radio" name="gender" value="w"> Женский</p>
            </div>
            <button type="submit" name="submit" class="btn-lt">регистрация</button>
            <a href="login" class="btn-rt" style="text-decoration: none">вход</a>

    </form>
</div>
</div>