<script src="/app/views/profile/scripts/login.js"></script>
<div class="header">
    <a href="#"></a>
</div>
<div class="frm">
    <form method="post"><br>
        <?php if ($error): ?>
            <div class="err" role="alert">
                <?= $error; ?>
            </div>
        <?php endif; ?>
        <input type="text" name="login" placeholder="введите логин:" value="<?php echo $login ?>"
               class="form_control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
               required>
        <input type="password" name="pass" placeholder="введите пароль:" value="<?php echo $password ?>"
               class="form_control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
               required>
        <button type="submit" name="submit" class="btn-lt">Войти</button>
        <a href="register" class="btn-rt" style="text-decoration: none">регистрация</a>
    </form>
</div>
