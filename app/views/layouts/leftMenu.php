<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
      integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="/app/views/layouts/style/leftMenu.css">
<div class="leftMenu">
    <div class="list-group">
        <a href="/profile/<?= $_SESSION['user']['login'] ?>" class="list-group-btn"><i class="fas fa-home"></i> <span>моя страница</span></a>
        <a href="/newsTape" class="list-group-btn"><i class="fas fa-comment-alt"></i> <span>новости</span></a>
        <a href="/messages" class="list-group-btn"><i class="fas fa-envelope"></i> <span>сообщения</span></a>
        <a href="/friend" class="list-group-btn"><i class="fas fa-user-friends"></i> <span>друзья</span></a>
        <a href="/photo/<?= $_SESSION['user']['login'] ?>" class="list-group-btn"><i class="fas fa-camera"></i> <span>фотографии</span></a>
        <a href="/users" class="list-group-btn"><i class="fas fa-users"></i> <span>люди</span></a>
    </div>
</div>