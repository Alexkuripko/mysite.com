<!DOCTYPE html>
<html>
<head>
    <title><?php use app\controllers\BaseController;

        $title = $this->addTitle(); ?></title>
    <link rel="stylesheet" href="/assets/build/css/style.css">
    <script src="/app/template/js/script.js"></script>
    <link rel="stylesheet" href="/app/lib/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/app/template/css/style.css"/>
    <link rel="stylesheet" href="/app/views/layouts/style/header.css"/>
</head>
<?php if (isset($_SESSION['user'])): ?>
<body>
<div class="header">
    <a href="#"></a>
    <div id="header_profile_menu" onclick="showBlock('#dropDown_menu', 400)">
        <b id="header_name"><?= $_SESSION['user']['name'] ?></b>
        <img src="/app/template/images/avatar/<?= $this->model->getUserData($_SESSION['user']['id'], 'id', 'avatar') ?>"
             alt="<?= $_SESSION['user']['name'] ?>" class="rounded-circle" width="40" height="40">
    </div>
</div>
<div id="dropDown_menu" style="display: none;">
    <a href="/profile/<?= $_SESSION['user']['login'] ?>">
        <div class="list_group-btn"><i class="fas fa-home"></i> моя страница</div>
    </a>
    <a href="/edit">
        <div class="list_group-btn"><i class="fas fa-edit"></i> редактировать</div>
    </a>
    <a href="/logout">
        <div class="list_group-btn"><i class="fas fa-sign-out-alt"></i> выход</div>
    </a>
</div>
<?php endif; ?>