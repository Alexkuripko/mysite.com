$(document).mouseup(function (e) {
    var block = $("#dropDown_menu");
    if (!block.is(e.target)
        && block.has(e.target).length === 0) {
        block.hide();
    }
});