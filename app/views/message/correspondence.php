<?php $_SESSION['last_id'] = $this->model->getUserData($login, 'login', 'id') ?>
<?php $_SESSION['last_login'] = $login ?>
<link rel="stylesheet" href="/app/views/message/style/messages.css">
<script src="/app/views/message/scripts/message.js"></script>
<script>$(document).ready(function () {
        scrollDown();
    });</script>
<div id="userLogin" style="display: none"><?= $login ?></div>
<div class="content-block content_corr">
    <div id="messTitle">
        <a href="/messages"><div id="back_btn" >
        <i class="fas fa-chevron-left"></i>
            </div></a>
        <a href="/profile/<?= $login ?>"><img id="title_avatar"
                                              src="/app/template/images/avatar/<?= $avatar ?>"
                                              class="rounded-circle" width="60" height="60"></a>
        <div id="title_name">
            <?= $name ?>
            <?= $lastname ?>
        </div>
        <div id="last_active">
            <?= \app\components\helpers\ProfileHelper::lastDateActive($date, $gender) ?>
        </div>
        <div class="messList" id="corrScroll">
            <?php include "app/views/message/corrList.php" ?>
        </div>
        <div id="text_area">
            <textarea id="write_mess" name="message"></textarea>
            <button id="send_mess" type="button" onclick="sendMessage()">отправить</button>
        </div>
    </div>
</div>