<link rel="stylesheet" href="/app/views/message/style/messages.css">
<div class="content-block content-dialogue">
    <?php foreach ($dialogueList as $order => $data): ?>
        <?php extract($data) ?>
        <?php if ($id_sender == $_SESSION['user']['id']) {
            $userId = $id_recipient;
        } else $userId = $id_sender;
        foreach ($userList as $index) {
            foreach ($index as $item => $value) {
                if ($item == 'id' && $value == $userId) {
                    extract($index);
                }
            }
        }
        ?>
        <a href="correspondence/<?= $login ?>" class="dialogue">
            <div class="message_block">
                <img id="dialogue_avatar"
                     src="/app/template/images/avatar/<?= $avatar ?>"
                     class="rounded-circle" width="60" height="60">
                <div class="dialogue_name"><?= $name ?>
                    <?= $lastname ?>
                </div>
                <img id="dialogue_avatar2"
                     src="/app/template/images/avatar/<?= $userList[$id_sender]['avatar'] ?>"
                     class="rounded-circle" width="25" height="25">
                <div class="preview">
                    <?= $preview ?>
                </div>
                <hr id="mess">

            </div>
        </a>
    <?php endforeach; ?>
    <?php if (!sizeof($dialogueList)): ?>
        <div>
            <p id="no_mess"> у вас пока нет сообщений</p><br>
            <a href="/friend" id="begin_mess">начните диалог с друзьями</a>
            <hr>
        </div>
    <?php endif; ?>
</div>