function getMessages() {
    var login = $('#userLogin').text();
    $.ajax({
        url: '/corrList',
        type: 'POST',
        data: {
            userLogin: login
        },
        success: function (data) {
            $("#corrScroll").html(data);
            scrollDown();
        }
    })
}

function sendMessage() {
    var message = $('#write_mess').val();
    $.ajax({
        url: '/sendMessage',
        type: 'POST',
        data: {
            message: message
        }
    }).done(function () {
        $('#write_mess').val('');
        getMessages();
    });
}

function scrollDown() {
    $('#corrScroll').scrollTop($('#corrScroll')[0].scrollHeight);
}

function showDellBtn(id) {
    $('#dell' + id).fadeIn();
    $('.send#mess' + id).css('background-color', '#636371');
    $('.recip#mess' + id).css('background-color', '#76a27e');
}

function cancelDellBtn(id) {
    $('#dell' + id).fadeOut();
    $('.send#mess' + id).css('background-color', '#c6c6d0');
    $('.recip#mess' + id).css('background-color', '#b6e2be');
}

function dellMessage(id) {
    $.ajax({
        url: '/dellMessage',
        type: 'POST',
        data: {
            id: id
        }
    });
    getMessages();
}