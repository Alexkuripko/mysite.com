<?php foreach ($messageList as $order => $data): ?>
    <?php extract($data) ?>
    <?php if ($id_send == $_SESSION['user']['id']): ?>
        <div class="mess send" id="mess<?= $id ?>" onclick="showDellBtn(<?= $id ?>)">
            <?= $mess ?>
        </div>
        <img id="send_avatar"
             src="/app/template/images/avatar/<?= $this->model->getUserData($id_send, 'id', 'avatar') ?>"
             class="rounded-circle" width="25" height="25">
        <div class="dell" id="dell<?= $id ?>">
            <i class="far fa-trash-alt " onclick="dellMessage(<?= $id ?>)"></i>
            <i class="fas fa-times cancelDell_1" onclick="cancelDellBtn(<?= $id ?>)"></i>
        </div>
    <?php else: ?>
        <div class="mess recip" id="mess<?= $id ?>" onclick="showDellBtn(<?= $id ?>)">
            <?= $mess ?>
        </div>
        <img id="recip_avatar"
             src="/app/template/images/avatar/<?= $this->model->getUserData($id_send, 'id', 'avatar') ?>"
             class="rounded-circle" width="25" height="25">
        <div class="dell" id="dell<?= $id ?>">
            <i class="far fa-trash-alt" onclick="dellMessage(<?= $id ?>)"></i>
            <i class="fas fa-times cancelDell_2" onclick="cancelDellBtn(<?= $id ?>)"></i>
        </div>
    <?php endif; ?>
<?php endforeach; ?>