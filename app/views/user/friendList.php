<div id="friend_title">Мои друзья
    <hr>
</div>
<?php $i=false?>
<?php foreach ($userList as $order => $data): ?>
    <?php extract($data) ?>
    <?php if ($status == 'friend'): ?>
        <div class="friend_elem">
            <a id="friend_img" href="profile/<?= $login ?>"><img src="app/template/images/avatar/<?= $avatar ?>"
                                                                 width="80"
                                                                 height="80" class="rounded-circle"></a>
            <div class="friend_name">
                <b> <?= $name ?> <?= $lastname ?></b>
            </div>
            <div id="active">
                <?= \app\components\helpers\ProfileHelper::lastDateActive($date_of_last_active, $gender) ?>
            </div>
            <p id="delFriend<?= $id ?>" class="friend-btn" onclick="delFriend(<?= $id ?>)">
                удалить
            </p>
            <a href="correspondence/<?= $login ?>">
                <div class="button send-btn">написать</div>
            </a>
        </div>
    <?php endif; ?>
<?php endforeach; ?>