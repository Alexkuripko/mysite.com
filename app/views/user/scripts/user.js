function delFriend(id) {
    $.ajax({
        url: 'delFriend',
        type: 'POST',
        data: {
            user: id
        }
    }).done(function () {
        $('#delFriend' + id).text('добавить в друзья').attr({
            'onclick': "addFriend(" + id + ")",
            'id': 'addFriend' + id
        });
    })
}

function confirmFriend(id) {
    $.ajax({
        url: 'confirmFriend',
        type: 'POST',
        data: {
            user: id
        }
    }).done(function () {
        $('#confirmFriend' + id).text('удалить').attr({'onclick': "delFriend(" + id + ")", 'id': 'delFriend' + id});
    })
}


function cancelRequest(id) {
    $.ajax({
        url: 'cancelRequest',
        type: 'POST',
        data: {
            user: id
        }
    }).done(function () {
        $('#cancelRequest' + id).text('добавить в дузья').attr({
            'onclick': "addFriend(" + id + ")",
            'id': 'addFriend' + id
        });
    })
}

function addFriend(id) {
    $.ajax({
        url: 'addFriend',
        type: 'POST',
        data: {
            user: id
        }
    }).done(function () {
        $('#addFriend' + id).text('отменить заявку').attr({
            'onclick': "cancelRequest(" + id + ")",
            'id': 'cancelRequest' + id
        });
    })
}

function loadFriendList() {
    $.ajax({
        url: "friendList",
        success: funcSuccess
    });
    setStyleBtn('friend');
}

function loadInRequest() {
    $.ajax({
        url: "friendInRequest",
        success: funcSuccess
    });
    setStyleBtn('in');
}

function loadOutRequest() {
    $.ajax({
        url: "friendOutRequest",
        success: funcSuccess
    });
    setStyleBtn('out');
}

function funcSuccess(data) {
    $(".content-users").html(data);
}

function setStyleBtn(atr) {
    $('#out-btn').css({
        'color': '#585858',
        'background-color': "white",
        'box-shadow': "0 0 12px rgba(84, 84, 84, 0.18)"
    });
    $('#in-btn').css({
        'color': '#585858',
        'background-color': "white",
        'box-shadow': "0 0 12px rgba(84, 84, 84, 0.18)"
    });
    $('#friend-btn').css({
        'color': '#585858',
        'background-color': "white",
        'box-shadow': "0 0 12px rgba(84, 84, 84, 0.18)"
    });

    $('#' + atr + '-btn').css({'color': '#ffffff', 'background-color': "#2f737e", 'box-shadow': "0 0 12px #2f737e"});
}
