<div id="friend_title">Входящие заявки
    <hr>
</div>
<?php foreach ($userList as $order => $data): ?>
    <?php extract($data) ?>
    <?php if ($status == 'in'): ?>
        <div class="friend_elem">
            <img id="friend_img" src="app/template/images/avatar/<?= $avatar ?>" width="80" height="80"
                 class="rounded-circle">
            <div class="friend_name">
                <b><?= $name ?> <?= $lastname ?></b>
            </div>
            <div id="active">
                <?= \app\components\helpers\ProfileHelper::lastDateActive($date_of_last_active, $gener) ?>
            </div>
            <p id="confirmFriend<?= $id ?>" class="friend-btn" onclick="confirmFriend(<?= $id ?>)">
                подтвердить
            </p>
            <a href="correspondence/<?= $login ?>">
                <div class="button send-btn">написать</div>
            </a>
        </div>
    <?php endif; ?>
<?php endforeach; ?>