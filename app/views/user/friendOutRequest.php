<div id="friend_title">Исходящие заявки<hr></div>
<?php foreach ($userList as $order => $data): ?>
    <?php extract($data) ?>
    <?php if ($status == 'out'): ?>
        <div class="friend_elem">
            <a id="friend_img" href="profile/<?= $login ?>"><img src="app/template/images/avatar/<?= $avatar ?>"
                                                                 width="80"
                                                                 height="80"
                                                                 class="rounded-circle"></a>
            <div class="friend_name">
                <b> <?= $name ?> <?= $lastname ?></b>
            </div>
            <div id="active">
                <?= \app\components\helpers\ProfileHelper::lastDateActive($date_of_last_active, $gender) ?>
            </div>
            <p id="cancelRequest<?= $id ?>" class="friend-btn" onclick="cancelRequest(<?= $id ?>)">отменить заявку
            </p>
            <a href="correspondence/<?=$login?>"><div class="button send-btn" >написать</div>
            </a>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
