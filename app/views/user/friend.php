<link rel="stylesheet" href="/app/views/user/style/user.css">
<script src="/app/views/user/scripts/user.js"></script>
<div class="content">
    <div class="content-block content-users">
        <?php include 'app/views/user/friendList.php'; ?>
    </div>
    <div id="friend_group-btn">
        <div class="friend_btn" id="friend-btn" onclick="loadFriendList()">
            <p>Мои друзья</p>
        </div>
        <div class="friend_btn" id="in-btn" onclick="loadInRequest()">
            <p>Входящие заявки</p>
        </div>
        <div class="friend_btn" id="out-btn" onclick="loadOutRequest()">
            <p>Исходщие заявки</p>
        </div>

    </div>
</div>
