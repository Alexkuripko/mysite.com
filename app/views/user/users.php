<link rel="stylesheet" href="/app/views/user/style/user.css">
<script src="/app/views/user/scripts/user.js"></script>
<div class="content">
    <div class="content-block content-users">
        <div id="friend_title">&emsp; Люди<hr></div>
        <?php foreach ($userList as $order => $data): ?>
            <?php extract($data) ?>
            <div class="friend_elem">
                <a id="friend_img" href="profile/<?= $login ?>"><img src="app/template/images/avatar/<?= $avatar ?>"
                                                                     width="80"
                                                                     height="80"
                                                                     class="rounded-circle"></a>
                <div class="friend_name">
                    <b> <?= $name ?> <?= $lastname ?></b>
                </div>
                <div id="active">
                    <?= \app\components\helpers\ProfileHelper::lastDateActive($date_of_last_active, $gender) ?>
                </div>
                <?= \app\components\helpers\UserHelper::getButton($status, $id)?>
                <a href="correspondence/<?=$login?>"><div class="button send-btn" >написать</div>
                </a>
            </div>
        <?php endforeach; ?>

    </div>
</div>
