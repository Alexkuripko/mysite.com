<?php

namespace app\models;

use app\lib\Db;
use PDO;
use app\components\validators\Validator;

abstract class BaseModel
{

    public function __construct()
    {
        $this->connection = Db::getConnection();
    }

    protected $connection;
    protected $errors = [];


    public function create($tableName, array $data)
    {
        $columns = array_keys($data);
        $values = array_map(function ($value) {
            if ($value == null) return 'null';
            return "'" . $value . "'";
        }, $data);

        $columns = implode(', ', $columns);
        $values = implode(', ', $values);
        $sql = 'INSERT INTO `' . $tableName . '`(' . $columns . ')VALUES(' . $values . ')';
        $this->connection->prepare($sql)->execute();
    }

    public function select($tableName, $data = '', $cell = '*', $order = '')
    {
        $column = array_keys($data);
        $value = array_values($data);
        $and = '';
        $and2 = '';
        $or = '';
        $lp = '';
        $rp = '';
        if (sizeof($data) > 1) $and = ' AND `' . $column[1] . '` LIKE \'' . $value[1] . '\'';

        if (isset($data[0])) {
            $col = array_keys($data[0]);
            $val = array_values($data[0]);
            $and2 = ' AND `' . $col[1] . '` LIKE \'' . $val[1] . '\'';
            $or = ' OR (`' . $col[0] . '` LIKE \'' . $val[0] . '\'';
            $lp = '(';
            $rp = ')';
        }

        if (is_array($order)) $order = ' ORDER BY `' . $order[0] . '` ' . $order[1];
        $stmt = $this->connection->prepare('SELECT ' . $cell . ' FROM `' . $tableName . '` WHERE ' . $lp . '`' . $column[0] . '` LIKE \'' . $value[0] . '\'' . $and . $rp . $or . $and2 . $rp . $order);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function update($tableName, $sets, $place)
    {
        $column = array_keys($place);
        $value = array_values($place);
        $and = '';
        if (sizeof($place) == 2) $and = ' AND `' . $column[1] . '` = \'' . $value[1] . '\'';
        $set = array_map(function ($key, $value) {
            return '`' . $key . "` = '" . $value . "' ";
        }, array_keys($sets), array_values($sets));
        $set = implode(',', $set);
        $result = $this->connection->prepare('UPDATE `' . $tableName . '` SET ' . $set . ' WHERE `' . $column[0] . '` = \'' . $value[0] . '\' ' . $and)->execute();
        return $result;
    }

    public function delete($tableName, $data)
    {
        $column = array_keys($data);
        $value = array_values($data);
        $and = '';
        if (sizeof($data) == 2) $and = ' AND `' . $column[1] . '` = \'' . $value[1] . '\'';

        return $this->connection->prepare('DELETE FROM `' . $tableName .
            '` WHERE `' . $column[0] . '` = \'' . $value[0] . '\'' . $and)->execute();
    }


    public function getErrors(): array
    {
        return $this->errors;
    }


    public function rules(): array
    {
        return [];
    }

    public function rulesEdit(): array
    {
        return [];
    }

    public function validate(array $attributes): bool
    {
        $rules = $this->rules();
        $this->errors = [];
        $valid = true;

        foreach ($rules as $field => $validators) {
            if (!array_key_exists($field, $attributes)) {
                $attributes[$field] = '';
            }
            foreach ($validators as $validator) {
                if (!$validator->validate($attributes[$field])) {
                    $valid = false;
                    $this->errors[$field] = $validator->messages;
                }
            }
        }

        return $valid;
    }

    public function validateEdit(array $attributes): bool
    {
        $rules = $this->rulesEdit();
        $this->errors = [];
        $valid = true;

        foreach ($rules as $field => $validators) {
            foreach ($validators as $validator) {
                if (!$validator->validate($attributes[$field])) {
                    $valid = false;
                    $this->errors[$field] = $validator->messages;
                }
            }
        }

        return $valid;
    }

    public function getUserList($data = false)
    {
        $result = $this->connection->prepare('SELECT * FROM `user`');
        $result->execute();
        $users = $result->fetchAll();
        foreach ($users as $userKey => $user) {
            foreach ($user as $fieldKey => $value) {
                $userList[$user['id']][$fieldKey] = $value;
            }
        }
        if (!$data) unset($userList[$_SESSION['user']['id']]);

        return $userList;
    }

    public function getOtherUsers()
    {
        $userList = $this->getUserList();
        unset($userList[$_SESSION['user']['id']]);
        return $userList;
    }

    public function getUserData($value, $column, $data)
    {
        $user = $this->select('user', [$column => $value]);
        extract($user[0]);
        switch ($data) {
            case 'id':
                return $id;
                break;
            case 'name':
                return $name;
                break;
            case 'lastname':
                return $lastname;
                break;
            case 'login':
                return $login;
                break;
            case 'date_of_last_active':
                return $date_of_last_active;
                break;
            case 'avatar':
                return $avatar;
                break;
            case 'gender':
                return $gender;
                break;
            case 'user':
                return $user;
                break;
        }
    }

    public static function getAvailableField()
    {
        return [
            'id', 'name', 'lastname','login','pass', 'born_date', 'gender'
        ];
    }

}
