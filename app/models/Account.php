<?php
namespace app\models;

use app\components\helpers\ProfileHelper;
use app\components\validators\ComparisonValidator;
use app\components\validators\ComparisonValidatorEdit;
use app\components\validators\MatchValidator;
use app\components\validators\RequiredValidator;
use app\components\validators\StringValidator;
use app\components\validators\UniqueLogin;

class Account extends BaseModel
{

    public function rules(): array
    {

        $stringValidator = new StringValidator(4, 25);
        return [
            'name' => [
                $stringValidator,
                new RequiredValidator(),
            ],
            'lastname' => [
                $stringValidator,
                //$matchValidator,
                new RequiredValidator(),
            ],
            'login' => [
                new StringValidator(6, 25),
                new MatchValidator('(^[a-zA-Z0-9]+$)'),
                new RequiredValidator(),
                new UniqueLogin(),
            ],
            'pass' => [
                new StringValidator(8, 25),
                new MatchValidator('(^[a-zA-Z0-9]+$)'),
                new RequiredValidator(),
            ],
            'pass2' => [
                new ComparisonValidator(),

            ],
        ];
    }


    public function checkLoginExists($login)
    {
        $result = $this->select('user', ['login' => $login]);
        if ($result) {
            return true;
        } else return false;

    }

    public function getDataByLogin($login)
    {
        $users = $this->checkLoginExists($login);
        return $users ?? null;
    }


    public function addNewUser($data)
    {
        $availableFieids = self::getAvailableField();
        $creationsData = [];
        $born_date = $data['born_year'] . '-' . $data['born_mounth'] . '-' . $data['born_day'];
        $data['born_date'] = $born_date;
        $data['pass'] = password_hash($data['pass'], PASSWORD_DEFAULT);
        foreach ($data as $key => $item) {
            foreach ($availableFieids as $field) {
                if ($key === $field) {
                    $creationsData[$key] = $item;
                }
           }
        }
        $this->create('user', $creationsData);
    }

    public function checkUserData($login, $password)
    {
        $user = $this->select('user', ['login' => $login]);
        $password = password_verify($password, $user[0]['pass']);
        if ($password) {
            return $user[0];
        }
        return false;
    }

    public function getUserById($id)
    {
        return $this->select('user', ['id' => $id]);
    }

    public function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    public function checkLogged()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        header("Location:/login");
    }
}