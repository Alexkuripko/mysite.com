<?php
namespace app\models;

class Message extends BaseModel
{

    static function getDateNow()
    {
        $date = date('Y-m-d G:i:s');
        return $date;
    }

    public function prepareSendData($data)
    {
        $sendData = [];
        $sendData['id_send'] = $_SESSION['user']['id'];
        $sendData['id_recip'] = $_SESSION['last_id'];
        $sendData['mess'] = $data['message'];
        $sendData['send_date'] = self::getDateNow();
        return $sendData;

    }

    public function sendMessage($sendData)
    {
        $this->create('message', $sendData);
    }

    public function updateDialogueInfo($data)
    {
        $id = $_SESSION['user']['id'];
        $id2 = $_SESSION['last_id'];
        $text = substr($data['message'], 0, 42);
        $date = self::getDateNow();
        $list = $this->select('dialogue', ['id_sender' => $id, 'id_recipient' => $id2]);
        if (!is_array($list)) $list = $this->select('dialogue', ['id_sender' => $id2, 'id_recipient' => $id]);
        if ($list == null) {
            $this->create('dialogue', ['id' => null, 'id_sender' => $id, 'id_recipient' => $id2, 'preview' => $text, 'last_date' => $date]);
        } else {
            $dId = $this->select('dialogue', ['id_sender' => $id, 'id_recipient' => $id2], 'id');
            if (!is_array($dId)) $dId = $this->select('dialogue', ['id_sender' => $id2, 'id_recipient' => $id], 'id');
            $dId = $dId[0]['id'];
            $this->update('dialogue', ['preview' => $text, 'last_date' => $date, 'id_sender' => $id, 'id_recipient' => $id2], ['id' => $dId]);
        }

    }

    public function getMessageList($id2)
    {
        $id = $_SESSION['user']['id'];
        $list = $this->select('message', ['id_send' => $id, 'id_recip' => $id2, ['id_send' => $id2, 'id_recip' => $id]]);
        return $list;
    }

    public function getDialogueList()
    {
        $id = $_SESSION['user']['id'];
        $list = $this->select('dialogue', ['id_sender' => $id], '*', ['last_date', 'DESC']);
        if (!is_array($list)) $list = $this->select('dialogue', ['id_recipient' => $id], '*', ['last_date', 'DESC']);
        return $list;
    }

    public function dellMessage($id)
    {
        $this->delete('message', ['id' => $id]);
    }
}