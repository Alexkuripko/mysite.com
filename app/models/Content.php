<?php
namespace app\models;

use app\components\Router;
use app\models\BaseModel;
use app\lib\Db;
use PDO;

class Content extends BaseModel
{
    public $errors = null;

    static function createFileName($name, $user)
    {
        $fileName = $user . '_' . $name;
        if ($name == 'photo') {
            $fileName = $fileName . date('_Y-m-d_G:i:s');
        }
        return $fileName;
    }

    static function getFileType($name)
    {
        if ($_FILES[$name]['type'] == 'image/jpeg') {
            $fileType = '.jpg';
        }
        return $fileType;
    }

    public function updateAvatarInfo()
    {

        $type = self::getFileType('avatar');
        $name = $_SESSION['user']['login'] . '_avatar' . $type;
        $id = $_SESSION['user']['id'];
        $this->update('user', ['avatar' => $name], ['id' => $id]);
    }

    public function addPhotoInDb($name)
    {
        $userId = $_SESSION['user']['id'];
        $date = date('Y-m-d G:i:s');
        $this->create('photo', ['id' => null, 'user_id' => $userId, 'photo_name' => $name, 'photo_date' => $date]);
    }

    public function loadFile($dir, $name)
    {
        $user = $_SESSION['user']['login'];

        if ($_FILES[$name]["size"] > 1024 * 3 * 1024) {
            $errors = 'слишком тяжелое фото';
            Router::redirect('');
            exit;
        }
        if ($_FILES[$name]["type"] != 'image/jpeg') {
            $errors = 'выберите фото формата JPEG';
            Router::redirect('');
            exit;
        }
        $baseDir = $_SERVER['DOCUMENT_ROOT'];
        $fileName = self::createFileName($name, $user);
        $fileType = self::getFileType($name);

        if (is_uploaded_file($_FILES[$name]["tmp_name"])) {
            move_uploaded_file($_FILES[$name]["tmp_name"], $baseDir . $dir . '/' . $fileName . $fileType);
        }
        return $fileName;
    }

    public function deleteFile($dir, $fileName)
    {
        $baseDir = $_SERVER['DOCUMENT_ROOT'];
        $path = $baseDir . $dir . $fileName . '.jpg';
        unlink($path);
    }

    public function getName($id)
    {
        $result = $this->select('user', ['id' => $id], 'name');
        foreach ($result as $order) {
            foreach ($order as $key => $value) {
                return $value;
            }
        }
    }

    public function dellPhotoOfDb($fileName)
    {
        $id = $_SESSION['user']['id'];
        $this->delete('photo', ['photo_name' => $fileName, 'user_id' => $id]);
    }

    public function getPhotos($id)
    {
        $photos = $this->select('photo', ['user_id' => $id], '*', ['photo_date', 'DESC']);
        return $photos;
    }

    public function putLikePhoto()
    {
        $id = $_SESSION['user']['id'];
        $photoId = $_POST['photoId'];
        $userId = $_POST['userId'];
        $res = $this->select('photo', ['id' => $photoId], 'likes');
        extract($res[0]);
        $likes = $likes + 1;
        $this->update('photo', ['likes' => $likes], ['id' => $photoId]);
        $this->create('like_photo', ['id' => null, 'photo_id' => $photoId, 'user_id' => $id, 'owner_id' => $userId]);
    }

    public function removeLikePhoto()
    {
        $id = $_SESSION['user']['id'];
        $photoId = $_POST['photoId'];
        $userId = $_POST['userId'];
        $this->delete('like_photo', ['photo_id' => $photoId, 'user_id' => $id]);
        $res = $this->select('likes', ['id' => $photoId]);
        extract($res[0]);
        $likes = $likes - 1;
        $this->update('photo', ['likes' => $likes], ['id' => $photoId]);
    }

    public function getPhotoLikes($id, $keyName)
    {
        $likes = $this->select('like_photo', [$keyName => $id]);
        return $likes;
    }
}