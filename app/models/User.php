<?php
namespace app\models;

use app\lib\Db;
use PDO;

class User extends BaseModel
{
    public function converseUserList($list)
    {
        foreach ($list as $userKey => $user) {
            foreach ($user as $fieldKey => $value) {
                $list[$user['id']]['status'] = $this->defineUserStatus($user['id']);
            }
        }
        return $list;
    }

    function addFriend($id2)
    {
        $id = $_SESSION['user']['id'];
        $this->create('user_friend', ['user2_id' => $id2, 'user1_id' => $id]);
    }

    function confirmFriend($id2)
    {
        $id = $_SESSION['user']['id'];
        $this->update('user_friend', ['status' => 1], ['user1_id' => $id2, 'user2_id' => $id]);
    }

    public function delFriend(int $id)
    {
        $id2 = $_SESSION['user']['id'];
        $this->delete('user_friend', ['user1_id' => $id2, 'user2_id' => $id]);
        $this->delete('user_friend', ['user1_id' => $id, 'user2_id' => $id2]);

    }

    private function getUserStatus($id2)
    {
        $id = $_SESSION['user']['id'];
        $result = $this->select('user_friend', ['user1_id' => $id, 'user2_id' => $id2], 'status');
        if (sizeof($result) < 1) $result = $this->select('user_friend', ['user1_id' => $id2, 'user2_id' => $id], 'status');
        if (sizeof($result) == 1) {
            if ($result[0]['status'] == 1) {
                return true;
            }
            return false;
        }
    }

    private function defineUserStatus($id)
    {
        $in = $this->getInRequestExists($id);
        $out = $this->getOutRequestExists($id);
        $status = $this->getUserStatus($id);
        if ($status == 1) {
            return 'friend';
        } elseif ($in) {
            return 'in';
        } elseif ($out) {
            return 'out';
        } else return 'none';

    }

    private function getOutRequestExists($id2)
    {
        $id = $_SESSION['user']['id'];
        return $result = $this->select('user_friend', ['user1_id' => $id, 'user2_id' => $id2]);
    }

    private function getInRequestExists($id2)
    {
        $id = $_SESSION['user']['id'];
        return $result = $this->select('user_friend', ['user1_id' => $id2, 'user2_id' => $id]);

    }
}