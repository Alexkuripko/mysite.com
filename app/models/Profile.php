<?php
namespace app\models;

use app\components\validators\ComparisonValidatorEdit;
use app\components\validators\MatchValidator;
use app\components\validators\StringValidator;

class Profile extends BaseModel
{

    public function rulesEdit(): array
    {
        $stringValidator = new StringValidator(3, 20);
        return [
            'name' => [
                $stringValidator,
//                new MatchValidator('(^[a-zA-Z]+$)'),
            ],
            'lastname' => [
                $stringValidator,
//                new MatchValidator('(^[a-zA-Z]+$)'),
            ],
            'new_pass' => [
                new ComparisonValidatorEdit(),
                new StringValidator(8, 20),
                new MatchValidator('(^[a-zA-Z0-9]+$)'),
            ],
        ];
    }

    public function edit($id,$name,$lastname,$password){
        $password = password_hash($password, PASSWORD_DEFAULT);
        $this->update('user', ['name' => $name, 'lastname'=> $lastname, 'pass' => $password],['id' => $id]);
    }
}