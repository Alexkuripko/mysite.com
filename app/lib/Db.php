<?php
namespace app\lib;

use PDO;

class Db
{

    protected $db;

    public static function getConnection()
    {

        $config = require 'app/config/db.php';
        $db = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['name'] . ';charset=utf8', $config['user'], $config['password']);
        if (!$db) {
            die("Connection failed: " . mysqli_connect_error());
        }
        return $db;
    }

    public function query($sql, $params = [])
    {
        $stat = $this->db->prepare($sql);
        if (!empty($params)) {
            foreach ($params as $key => $val) {
                $stat->bindValue(':' . $key, $val);
            }
        }
        $stat->execute();
        return $stat;
    }

    public function row($sql)
    {
        $result = $this->query($sql, $params = []);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function column($sql, $params = [])
    {
        $result = $this->query($sql, $params);
        return $result->fetchColumn();
    }
}