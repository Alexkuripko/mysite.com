function showBlock(id, speed) {
    if (!speed) speed = 0;
    $(id).fadeIn(speed)
}

function hideBlock(id, speed) {
    if (!speed) speed = 0;
    $(id).fadeOut(speed)
}