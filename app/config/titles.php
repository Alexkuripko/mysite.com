<?php

return [
    'register' => 'Регистрация',
    'login' => 'Логин',
    'profile' => 'Профиль',
    'edit' => 'Редактирование',
    'users' => 'Люди',
    'messages' => 'Диалоги',
    'newsTape' => 'Новости',
    'friends' => 'Друзья',
    'correspondence' => 'Переписка',
    'photo' => 'Фотографии',
    '' => 'Главная',
];