<?php

require 'app/lib/Dev.php';

use app\components\Router;

header('Content-Type: text/html; charset=utf-8');
spl_autoload_register(function ($class) {
    $path = str_replace('\\', '/', $class . '.php');
    if (file_exists($path)) {
        require $path;
    }
});

session_start();
$router = new Router();
$router->run();